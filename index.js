const csv = require("csv-parser");
const fs = require("fs");
const results = [];

// node index.js
// prettier --write "**/*.json"

// CSV reading
fs.createReadStream("./dataset.csv")
  .pipe(csv())
  .on("data", (data) => results.push(data))
  .on("end", () => {
    //
    // ----- CSV Model -----
    //  ID,Medida
    //  1,unidade,
    //  2,22
    // ------- Output -------
    //
    //   { NAME: 'Daffy Duck', AGE: '24' },
    //   { NAME: 'Bugs Bunny', AGE: '22' }

    let measures = [];
    results
      .map((line) => line.Medida) // the columns are 'ID' and 'Medida' (each line have this two "properties")
      .join(",") // put each element in a string separated by comma
      .split(",") // split a string into an array of substrings and returns the new array
      .forEach((measure) => {
        // if don't have this measure in array(measures[]), insert this in array(measures.push)
        if (!measures.includes(measure.replace(/\s/g, "")))
          // special characters
          measures.push(measure.replace(/\s/g, ""));
      });
    // at this point we have an array (measures[]) with all measures and without repetition
    measures = measures.filter((measure) => measure !== ""); // remove empty
    const measures_json = {};
    measures.forEach(
      (measure, index) =>
        (measures_json[index + 1] = { name: measure, abr: "" }) // index and objects
    );

    // turn this object (json) into a string and record this in a file(measures.json)
    fs.writeFileSync("measures.json", JSON.stringify(measures_json));
    const subcategories_json = results.map((line) => {
      const ids = [];
      line.Medida.split(",").map((measure) => {
        const id = measures
          .map(
            (current, index) =>
              current.replace(/\s/g, "") === measure.replace(/\s/g, "") // ?
                ? index + 1 // ?
                : "none" // ?
          )
          .filter((current) => current !== "none");

        ids.push(id[0]);
      });
      return { sub_id: line.ID, measures: ids };
    });

    fs.writeFileSync(
      "subcategories.json",
      JSON.stringify({ subcategories: subcategories_json })
    );
  });
